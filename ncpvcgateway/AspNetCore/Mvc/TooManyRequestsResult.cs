﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace ncpvcgateway.AspNetCore.Mvc
{
    /// <summary>
    /// An Microsoft.AspNetCore.Mvc.ObjectResult that when executed will produce a TooManyRequests
    /// (429) response.
    /// </summary>
    [DefaultStatusCode(429)]
    public class TooManyRequestsResult : ObjectResult
    {
        /// <summary>
        /// Creates a new <see cref="TooManyRequestsResult "/> instance.
        /// </summary>
        /// <param name="value"></param>
        public TooManyRequestsResult([ActionResultObjectValue] object value) : base(value)
        {
        }
    }
}
