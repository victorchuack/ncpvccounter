namespace ncpvcweather.AspNetCore.Mvc.Models
{
    public class ApiError
    {
        public class ErrorDescription
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public string CorrelationId { get; set; }
        }
        public ErrorDescription Error { get; set; }

        public ApiError()
        {
            this.Error = new ErrorDescription();
        }
    }
}
